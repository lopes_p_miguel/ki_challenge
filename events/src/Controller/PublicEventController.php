<?php
  
namespace App\Controller;
  
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Event;
  
/**
 * @Route("/api", name="api_")
 */
class PublicEventController extends AbstractController
{
	public function __construct(private ManagerRegistry $doctrine) {}
    /**
     * @Route("/", name="publicEvent_index", methods={"GET"})
     */
    public function index(): Response
    {
        $events = $this->doctrine
            ->getRepository(Event::class)
            ->createQueryBuilder('e')
			->where('e.candidate IS NULL')
			->orWhere('e.candidate = :empty')
			->setParameter('empty', '')
			->addOrderBy('e.eventDate', 'DESC')
			->addOrderBy('e.start', 'ASC')
			->addOrderBy('e.end', 'ASC')
			->getQuery()
			->getResult();
        $data = [];
        foreach ($events as $event) {
           $data[] = [
               'id' => $event->getId(),
               'start' => $event->getStart(),
               'end' => $event->getEnd(),
               'eventDate' => $event->getEventDate(),
           ];
        }
        return $this->json($data);
    }
  
    /**
     * @Route("/{id}", name="publicEvent_edit", methods={"PUT", "PATCH"})
     */
    public function edit(Request $request, int $id): Response
    {
        $entityManager = $this->doctrine->getManager();
        $event = $entityManager->getRepository(Event::class)->find($id);
        if (!$event) {
            return $this->json('No event found for id' . $id, 404);
        }
        $content = json_decode($request->getContent());
        $event->setCandidate($content->candidate);
        $entityManager->flush();
        $data =  [
            'id' => $event->getId(),
		    'candidate' => $event->getCandidate(),
        ];
        return $this->json($data);
    }
}
