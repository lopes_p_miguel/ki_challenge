<?php
  
namespace App\Controller;
  
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Event;
  
/**
 * @Route("/api", name="api_")
 */
class EventController extends AbstractController
{
	public function __construct(private ManagerRegistry $doctrine) {}
    /**
     * @Route("/event", name="event_index", methods={"GET"})
     */
    public function index(): Response
    {
        $products = $this->doctrine
            ->getRepository(Event::class)
            ->findBy(array(), array('eventDate' => 'DESC', 'start' => 'ASC', 'end' => 'ASC', 'interviewer' => 'ASC'));
        $data = [];
        foreach ($products as $product) {
           $data[] = [
               'id' => $product->getId(),
               'name' => $product->getName(),
               'start' => $product->getStart(),
               'end' => $product->getEnd(),
               'eventDate' => $product->getEventDate(),
               'interviewer' => $product->getInterviewer(),
               'candidate' => $product->getCandidate(),
           ];
        }
        return $this->json($data);
    }
  
    /**
     * @Route("/event", name="event_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $entityManager = $this->doctrine->getManager();
        $event = new Event();
        $event->setName($request->request->get('name'));
		$event->setStart($request->request->get('start'));
        $event->setEnd($request->request->get('end'));
        $event->setEventDate($request->request->get('eventDate'));
        $event->setInterviewer($request->request->get('interviewer'));
        $event->setCandidate($request->request->get('candidate'));
        $entityManager->persist($event);
        $entityManager->flush();
        return $this->json('Created new event successfully with id ' . $event->getId());
    }
  
    /**
     * @Route("/event/{id}", name="event_show", methods={"GET"})
     */
    public function show(int $id): Response
    {
        $event = $this->doctrine
            ->getRepository(Event::class)
            ->find($id);
        if (!$event) {
            return $this->json('No event found for id' . $id, 404);
        }
        $data =  [
            'id' => $event->getId(),
            'name' => $event->getName(),
		    'start' => $event->getStart(),
		    'end' => $event->getEnd(),
		    'eventDate' => $event->getEventDate(),
		    'interviewer' => $event->getInterviewer(),
		    'candidate' => $event->getCandidate(),
        ];
        return $this->json($data);
    }
  
    /**
     * @Route("/event/{id}", name="event_edit", methods={"PUT", "PATCH"})
     */
    public function edit(Request $request, int $id): Response
    {
        $entityManager = $this->doctrine->getManager();
        $event = $entityManager->getRepository(Event::class)->find($id);
        if (!$event) {
            return $this->json('No event found for id' . $id, 404);
        }
        $content = json_decode($request->getContent());
        $event->setName($content->name);
        $event->setStart($content->start);
        $event->setEnd($content->end);
        $event->setEventDate($content->eventDate);
        $event->setInterviewer($content->interviewer);
        $event->setCandidate($content->candidate);
        $entityManager->flush();
        $data =  [
            'id' => $event->getId(),
            'name' => $event->getName(),
		    'start' => $event->getStart(),
		    'end' => $event->getEnd(),
		    'eventDate' => $event->getEventDate(),
		    'interviewer' => $event->getInterviewer(),
		    'candidate' => $event->getCandidate(),
        ];
        return $this->json($data);
    }
  
    /**
     * @Route("/event/{id}", name="event_delete", methods={"DELETE"})
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->doctrine->getManager();
        $event = $entityManager->getRepository(Event::class)->find($id);
        if (!$event) {
            return $this->json('No event found for id' . $id, 404);
        }
        $entityManager->remove($event);
        $entityManager->flush();
        return $this->json('Deleted a event successfully with id ' . $id);
    }
}