<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $eventDate;

    #[ORM\Column(type: 'string', length: 255)]
    private $start;

    #[ORM\Column(type: 'string', length: 255)]
    private $end;

    #[ORM\Column(type: 'string', length: 255)]
    private $interviewer;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $candidate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEventDate(): ?string
    {
        return $this->eventDate;
    }

    public function setEventDate(string $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(string $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?string
    {
        return $this->end;
    }

    public function setEnd(string $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getInterviewer(): ?string
    {
        return $this->interviewer;
    }

    public function setInterviewer(string $interviewer): self
    {
        $this->interviewer = $interviewer;

        return $this;
    }

    public function getCandidate(): ?string
    {
        return $this->candidate;
    }

    public function setCandidate(?string $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }
}
