import React, { useState } from 'react';
import { Link, useParams } from "react-router-dom";
import Layout from "../components/Layout"
import Swal from 'sweetalert2'
import axios from 'axios';
import { validatePublicEvent } from './const.js';
  
function PublicEventEdit() {
    const [id, setId] = useState(useParams().id)
    const [name, setName] = useState('');
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [eventDate, setEventDate] = useState('')
    const [interviewer, setInterviewer] = useState('')
    const [candidate, setCandidate] = useState('')
    const [isSaving, setIsSaving] = useState(false)
  
   	const handleSave = () => {
		if (validatePublicEvent(candidate)) {
			setIsSaving(true);
			axios.patch(`/api/${id}`, {
				candidate: candidate
			})
			.then(function (response) {
				Swal.fire({
					icon: 'success',
					title: 'Event updated successfully!',
					showConfirmButton: false,
					timer: 1500
				})
				setIsSaving(false);
			})
			.catch(function (error) {
				Swal.fire({
					icon: 'error',
					title: 'An Error Occured!',
					showConfirmButton: false,
					timer: 1500
				})
				setIsSaving(false)
			})
		}
    }
  
  
    return (
        <Layout>
            <div className="container">
                <h2 className="text-center mt-5 mb-3">Edit Event</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-info float-right"
                            to="/">View All Events
                        </Link>
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label htmlFor="candidate">Candidate name</label>
                                <input 
                                    onChange={(event)=>{setCandidate(event.target.value)}}
                                    value={candidate}
                                    type="text"
                                    className="form-control"
                                    id="candidate"
                                    name="candidate"/>
                            </div>
                            <button 
                                disabled={isSaving}
                                onClick={handleSave} 
                                type="button"
                                className="btn btn-outline-success mt-3">
                                Update Event
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default PublicEventEdit;