import React,{ useState, useEffect} from 'react';
import { Link } from "react-router-dom";
import Layout from "../components/Layout"
import Swal from 'sweetalert2'
import axios from 'axios';
 
function EventList() {
    const  [eventList, setEventList] = useState([])
  
    useEffect(() => {
        fetchEventList()
    }, [])
  
    const fetchEventList = () => {
        axios.get('/api/event')
        .then(function (response) {
          setEventList(response.data);
        })
        .catch(function (error) {
          console.log(error);
        })
    }
  
    const handleDelete = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`/api/event/${id}`)
                .then(function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Event deleted successfully!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    fetchEventList()
                })
                .catch(function (error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'An Error Occured!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                });
            }
          })
    }
  
    return (
        <Layout>
           <div className="container">
            <h2 className="text-center mt-5 mb-3">Interviewer's view</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-primary"
                            to="/event/create">Create New Event
                        </Link>
                    </div>
                    <div className="card-body">
              
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Event name</th>
                                    <th>Date</th>
                                    <th>Start time</th>
                                    <th>End time</th>
                                    <th>Interviewer name</th>
                                    <th>Candidate name</th>
                                    <th width="240px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {eventList.map((event, key)=>{
                                    return (
                                        <tr key={key}>
                                            <td>{event.name}</td>
                                            <td>{event.eventDate}</td>
                                            <td>{event.start}</td>
                                            <td>{event.end}</td>
                                            <td>{event.interviewer}</td>
                                            <td>{event.candidate}</td>
                                            <td>
                                                <Link
                                                    to={`/event/show/${event.id}`}
                                                    className="btn btn-outline-info mx-1">
                                                    Show
                                                </Link>
                                                <Link
                                                    className="btn btn-outline-success mx-1"
                                                    to={`/event/edit/${event.id}`}>
                                                    Edit
                                                </Link>
                                                <button 
                                                    onClick={()=>handleDelete(event.id)}
                                                    className="btn btn-outline-danger mx-1">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default EventList;