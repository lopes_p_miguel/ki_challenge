import React,{ useState, useEffect} from 'react';
import { Link } from "react-router-dom";
import Layout from "../components/Layout"
import Swal from 'sweetalert2'
import axios from 'axios';
 
function PublicEventList() {
    const  [publicEventList, setPublicEventList] = useState([])
  
    useEffect(() => {
        fetchPublicEventList()
    }, [])
  
    const fetchPublicEventList = () => {
        axios.get('/api')
        .then(function (response) {
          setPublicEventList(response.data);
        })
        .catch(function (error) {
          console.log(error);
        })
    }
  
    return (
        <Layout>
           <div className="container">
            <h2 className="text-center mt-5 mb-3">Candidate's view</h2>
                <div className="card">
                    <div className="card-body">
              
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Start time</th>
                                    <th>End time</th>
                                    <th width="240px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {publicEventList.map((event, key)=>{
                                    return (
                                        <tr key={key}>
                                            <td>{event.eventDate}</td>
                                            <td>{event.start}</td>
                                            <td>{event.end}</td>
                                            <td>
                                                <Link
                                                    className="btn btn-outline-success mx-1"
                                                    to={`/edit/${event.id}`}>
                                                    Edit
                                                </Link>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default PublicEventList;