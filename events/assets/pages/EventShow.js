import React, {useState, useEffect} from 'react';
import { Link, useParams } from "react-router-dom";
import Layout from "../components/Layout"
import axios from 'axios';
  
function EventShow() {
    const [id, setId] = useState(useParams().id)
    const [event, setEvent] = useState({name:'', start:'', end:'', eventDate:'', interviewer:'', candidate:''})
    useEffect(() => {
        axios.get(`/api/event/${id}`)
        .then(function (response) {
          setEvent(response.data)
        })
        .catch(function (error) {
          console.log(error);
        })
    }, [])
  
    return (
        <Layout>
           <div className="container">
            <h2 className="text-center mt-5 mb-3">Show Event</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-info float-right"
                            to="/event"> View All Events
                        </Link>
                    </div>
                    <div className="card-body">
                        <b className="text-muted">Event name:</b>
                        <p>{event.name}</p>
                        <b className="text-muted">Date:</b>
                        <p>{event.eventDate}</p>
                        <b className="text-muted">Start time:</b>
                        <p>{event.start}</p>
                        <b className="text-muted">End time:</b>
                        <p>{event.end}</p>
                        <b className="text-muted">Interviewer:</b>
                        <p>{event.interviewer}</p>
                        <b className="text-muted">Candidate:</b>
                        <p>{event.candidate}</p>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default EventShow;