import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import Layout from "../components/Layout"
import Swal from 'sweetalert2'
import axios from 'axios';
import { validateEvent } from './const.js';

function EventCreate() {
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('')
    const [eventDate, setEventDate] = useState('')
    const [interviewer, setInterviewer] = useState('')
    const [candidate, setCandidate] = useState('')
    const [isSaving, setIsSaving] = useState(false)
  
	const handleSave = () => {
		if (validateEvent(eventDate, start, end, interviewer)) {
			setIsSaving(true);
			let formData = new FormData()
			formData.append("name", name)
			formData.append("start", start)
			formData.append("end", end)
			formData.append("eventDate", eventDate)
			formData.append("interviewer", interviewer)
			formData.append("candidate", candidate)
			axios.post('/api/event', formData)
			  .then(function (response) {
				Swal.fire({
					icon: 'success',
					title: 'Event saved successfully!',
					showConfirmButton: false,
					timer: 1500
				})
				setIsSaving(false);
				setName('')
				setStart('')
				setEnd('')
				setEventDate('')
				setInterviewer('')
				setCandidate('')
			  })
			  .catch(function (error) {
				Swal.fire({
					icon: 'error',
					title: 'An Error Occured!',
					showConfirmButton: false,
					timer: 1500
				})
				setIsSaving(false)
			  })
		}
   }

   return (
        <Layout>
            <div className="container">
                <h2 className="text-center mt-5 mb-3">Create New Event</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-info float-right"
                            to="/event">View All Events
                        </Link>
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Event name</label>
                                <input 
                                    onChange={(event)=>{setName(event.target.value)}}
                                    value={name}
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    name="name"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="eventDate">Date (YYYY-MM-DD)</label>
                                <input 
                                    onChange={(event)=>{setEventDate(event.target.value)}}
                                    value={eventDate}
                                    type="text"
                                    className="form-control"
                                    id="eventDate"
                                    name="eventDate"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="start">Start time (HH:00)</label>
                                <input 
									onChange={(event)=>{setStart(event.target.value)}}
                                    value={start}
                                    type="text"
                                    className="form-control"
                                    id="start"
                                    name="start"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="end">End time (HH:00)</label>
                                <input 
                                    onChange={(event)=>{setEnd(event.target.value)}}
                                    value={end}
                                    type="text"
                                    className="form-control"
                                    id="end"
                                    name="end"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="interviewer">Interviewer name</label>
                                <input 
                                    onChange={(event)=>{setInterviewer(event.target.value)}}
                                    value={interviewer}
                                    type="text"
                                    className="form-control"
                                    id="interviewer"
                                    name="interviewer"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="candidate">Candidate name</label>
                                <input 
                                    onChange={(event)=>{setCandidate(event.target.value)}}
                                    value={candidate}
                                    type="text"
                                    className="form-control"
                                    id="candidate"
                                    name="candidate"/>
                            </div>
                            <button 
                                disabled={isSaving}
                                onClick={handleSave} 
                                type="button"
                                className="btn btn-outline-primary mt-3">
                                Save Event
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default EventCreate;