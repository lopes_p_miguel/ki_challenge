import Swal from 'sweetalert2'

const validTime = new RegExp(
   '^(2[0-3]|[01]?[0-9]):(?:[0][0])$'
);
export const validateEvent = (requetsDate, requestStart, requestEnd, requestInterviewer) => {
	var dateYear = requetsDate.split("-")[0];
	var dateMonth = requetsDate.split("-")[1];
	var dateDay = requetsDate.split("-")[2];
	var dateS = dateYear + '-' + dateMonth + '-' + dateDay;
	var date = new Date(dateYear,dateMonth-1,dateDay);
	var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
		month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
	var dateC = year + '-' + month + '-' + day;
	if (dateS != dateC)	{
		Swal.fire({
			icon: 'error',
			title: 'Invalid date (YYYY-MM-DD)',
			showConfirmButton: false,
			timer: 1500
		})
		return false;
	}
	if (!validTime.test(requestStart) || !validTime.test(requestEnd)) {
		Swal.fire({
		icon: 'error',
		title: 'Invalid time (HH:00)',
		showConfirmButton: false,
		timer: 1500
		})
		return false;
	}
	var startHour = parseInt(requestStart.split(":")[0]);
	var endHour = parseInt(requestEnd.split(":")[0]);
	if ((startHour+1) != endHour)
	{
		Swal.fire({
		icon: 'error',
		title: 'One hour slot!',
		showConfirmButton: false,
		timer: 5500
		})
		return false;
	}
	if (requestInterviewer === "") {
		Swal.fire({
			icon: 'error',
			title: 'Interviewer name expected',
			showConfirmButton: false,
			timer: 1500
		})
		return false;
	}
	return true;
}
export const validatePublicEvent = (requetsCandidate) => {
	if (requetsCandidate === "") {
		Swal.fire({
			icon: 'error',
			title: 'Candidate name expected',
			showConfirmButton: false,
			timer: 1500
		})
		return false;
	}
	return true;
}