import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
  } from "react-router-dom";
import PublicEventList from "./pages/PublicEventList"
import PublicEventEdit from "./pages/PublicEventEdit"
import EventList from "./pages/EventList"
import EventCreate from "./pages/EventCreate"
import EventEdit from "./pages/EventEdit"
import EventShow from "./pages/EventShow"
  
function Main() {
    return (
        <Router>
            <Routes>
                <Route exact path="/"  element={<PublicEventList/>} />
                <Route path="/edit/:id"  element={<PublicEventEdit/>} />
                <Route path="/event"  element={<EventList/>} />
                <Route path="/event/create"  element={<EventCreate/>} />
                <Route path="/event/edit/:id"  element={<EventEdit/>} />
                <Route path="/event/show/:id"  element={<EventShow/>} />
            </Routes>
        </Router>
    );
}
  
export default Main;
  
if (document.getElementById('app')) {
    ReactDOM.render(<Main />, document.getElementById('app'));
}