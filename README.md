#Extra (using a database server Mariadb 10.5.9):
- create database events
- execute events-db.sql

#Running app
1) GO TO events
2) EDIT events\.env -> DATABASE_URL="mysql://root:pass@127.0.0.1:3306/events?serverVersion=mariadb-10.5.9"
3) RUN symfony server:start
4) OPEN http://127.0.0.1:8000
5) OPEN http://127.0.0.1:8000/event

#Candidate's view
Show the availables events, than the candidates can choose, by editing with her name.

#Interviewer's view
Allow the interviewer to manage the events: list, create, edit and see.